# Cellular automate engine


## Dependencies

+ raylib (https://github.com/raysan5/raylib)
+ GNU Make

It is also dynamically linked with the standard C math library,
make sure to add `-lm` if you're building manually.

## Build instructions

Just run make

```
make
```
